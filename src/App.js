import logo from './logo.svg';
import { useContext } from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './App.css';
import Layout from './pages/Layout';
import Home from './pages/Home';
import NoPage from './pages/NoPage';
import LoginPage from './pages/Login';
import RegisterPage from './pages/Register';
import { UserContext } from './auth/Context';
import CreateTaskPage from './pages/CreateTask';
import EditTaskPage from './pages/EditTask';
import Account from './pages/Account';

function App() {
  const { user } = useContext(UserContext);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          {user ? 
            <> 
              <Route index element={<Home />} />
              <Route path='/create/task' element={<CreateTaskPage />} />
              <Route path='/edit/task/:id' element={<EditTaskPage />} />
              <Route path='/account' element={<Account />} />
            </>
          : 
            <>
              <Route index element={<LoginPage />} />
              <Route path='login' element={<LoginPage />} />
              <Route path='register' element={<RegisterPage />} />
            </>
          }
          <Route path="*" element={<NoPage />} />
        </Route>

      </Routes>
    </BrowserRouter>
  );
}

export default App;
