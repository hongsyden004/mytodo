import { useContext, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { UserContext } from "../auth/Context";
import axios from 'axios';

const LoginPage = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const {submitting, dispatch} = useContext(UserContext);
    const navigate = useNavigate();

    const doLogin = async (e) => {
        e.preventDefault();
        if(email == '') {
            alert('Pls insert email!') 
            return;
        }
        if(password == '') {
            alert('Pls insert password!')
            return;
        }
        dispatch({type: 'LOGIN_START', userdata: null });
        const resultLogin = await requstLoginAPI({email, password});
        if(resultLogin) 
        {
            dispatch({type: 'LOGIN_SUCESS', userdata: resultLogin });
            navigate('/')
        }
    }

    const requstLoginAPI = async (datas) => {
        let res = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/login', datas, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(({data}) => {
            console.log(data)
            return data;
        })
        .catch(function (error) {
            // handle error
            console.log("error====");
            console.log(error);
        })
        return res;
    }

    return (
        <>
            <div className="container">
                <center>
                    <img src="logo512.png" style={styleCss.logo} />
                </center>
                <form onSubmit={doLogin}>
                    <div className="mb-3 mt-3">
                        <label htmlFor="email"> <span style={styleCss.requiredTex}>*</span> Email:</label>
                        <input autoComplete="off" type="email" className="form-control" id="email" placeholder="Enter email" name="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="pwd"> <span className="text-danger">*</span> Password:</label>
                        <input autoComplete="off" type="password" className="form-control" id="pwd" placeholder="Enter password" name="pswd"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <div className="d-grid">
                        {submitting ? 
                            <>
                                <button className="btn btn-primary" disabled>
                                    <span className="spinner-grow spinner-grow-sm"></span>
                                    Loading..
                                </button>
                            </>
                        : 
                        <>
                            <button type="submit" className="btn btn-primary btn-block">Login</button>
                        </>}
                    
                    </div>
                </form>
                <p>You don't have account yet! <Link to='/register'>Sign-up</Link></p>
            </div>
            
        </>
    )
};

const styleCss = {
    logo: {
        width: '30%'
    },
    requiredTex: {
        color: 'red',
    }
}
  
export default LoginPage;