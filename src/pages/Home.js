import $ from 'jquery';
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../auth/Context";
import HeaderPage from "../inc/Header";
import { Link } from "react-router-dom";
import axios from 'axios';
var moment = require('moment'); // require

const Home = () => {
    const [listDescription, setlistDescription] = useState([]);
    const [itemEdit, setItemEdit] = useState();
    const [taskFilter, setTaskFilter] = useState(1);
    const [sLimit, setSlimit] = useState(1);
    const [sSkip, setSskip] = useState(0);
    const [totalData, setTotalData] = useState();
    const { user, dispatch } = useContext(UserContext);
    console.log(moment(new Date).format('YYYY-MM-DD'));
   
    useEffect(() => {
        var filter = '';
        if(taskFilter === 2) {
            filter = '&completed=flase';
        }
        if(taskFilter === 3) {
            filter = '&completed=true';
        }
        // ?limit=2&skip=10&completed=true
        var slmitfilter = '?limit='+sLimit+'&skip='+sSkip+filter;

        const requstCreateTaskAPI = async () => {
            $('.loader').show();
            await axios.get('https://api-nodejs-todolist.herokuapp.com/task'+slmitfilter, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+user.token
                }
            }).then(({data}) => {
                console.log(data)
                setlistDescription(data.data)
                setTotalData(data.count)
                // return data;
                $('.loader').hide();
            })
            .catch(function (error) {
                // handle error
                console.log("error====");
                console.log(error);
                if(error.response.data.error == 'Please authenticate.') {
                    $('.loader').hide();
                    dispatch({type: 'LOGOUT'});
                }
            })
        }

        requstCreateTaskAPI();
    }, [user, taskFilter, sLimit]);

    const updateTaskByID = async (val) => {
        if(val)
        {
            $('.loader').show();
            let res = await axios.put(`https://api-nodejs-todolist.herokuapp.com/task/${val._id}`, { "completed": !val.completed }, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+user.token
                }
            }).then(({data}) => {
                console.log(data)
                return data;
            })
            .catch(function (error) {
                // handle error
                console.log("error====");
                console.log(error);
            })

            if(res.success) {
                setlistDescription(
                    listDescription.map((todo) => {
                        if (todo._id === val._id) {
                            return { ...val, completed: res.data.completed };
                        } else {
                            return todo;
                        }
                    })
                )
                $('.loader').hide();
                // setlistDescription(
                //     listDescription.map((des) => des._id === val._id ? { ...val, completed: res.data.completed } : des)
                // )
            }
        }
    }

    const editDelete = (val) => {
        setItemEdit(val._id);
        $('#nav-edit-delete').show();
    }

    const deleteItem = async () => {
        if(itemEdit) 
        {
            $('.loader').show();
            let res = await axios.delete(`https://api-nodejs-todolist.herokuapp.com/task/${itemEdit}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+user.token
                }
            }).then(({data}) => {
                console.log(data)
                return data;
            })
            .catch(function (error) {
                // handle error
                console.log("error====");
                console.log(error);
            });

            if(res !== undefined && res.success)
            {
                setlistDescription
                (
                    listDescription.filter((task) => task._id !== itemEdit)
                )
                $('.loader').hide();
                $('#nav-edit-delete').hide();
            }
        }
    }

    const colseButtonEditDelete = () => {
        $('#nav-edit-delete').hide();
    }

    // const loadMoreData = () => {
    //     setSlimit(sLimit + 1)
        
    // }
    // console.log("totalData");
    // console.log(totalData);

    // console.log("sLimit");
    // console.log(sLimit);

    // sLimit = 7
    // totalData = 6

    // if(sLimit > totalData){
    //     console.log("Hide");
    // }

    return(
        <>
            <div className="container-fluid">
                
                <HeaderPage title="My TODO List" icon="fas fa-plus-circle" link="/create/task" />

                <div className="form-check">
                    <input type="radio" className="form-check-input" id="radio1" name="optradio" value="1" checked={taskFilter == 1 ? 'checked': ''}  onClick={() => setTaskFilter(1)} />
                    <label className="form-check-label" htmlFor="radio1">All</label>
                </div>
                <div className="form-check">
                    <input type="radio" className="form-check-input" id="radio2" name="optradio" value="2" checked={taskFilter == 2 ? 'checked': ''} onClick={() => setTaskFilter(2)} />
                    <label className="form-check-label" htmlFor="radio2">Pending</label>
                </div>
                <div className="form-check">
                    <input type="radio" className="form-check-input" id="radio3" name="optradio" value="3" checked={taskFilter == 3 ? 'checked': ''} onClick={() => setTaskFilter(3)} />
                    <label className="form-check-label" htmlFor="radio3">Completed</label>
                </div>

                <div className="row mt-4">
                    <div className="col-12">
                            
                        <ul className="list-group">

                            {listDescription.map((val, index) => {
                                return(
                                    <>
                                        <li className="list-group-item d-flex justify-content-between align-items-center">
                                            <p onClick={() => editDelete(val)}> 
                                                {val.description}
                                                <br/>
                                                <div className="small">
                                                    { moment(val.createdAt).format('DD-MM-YYYY') } 
                                                </div>
                                            </p> 
                                            
                                            <span onClick={() => updateTaskByID(val)}>
                                                {val.completed ? 
                                                    <>
                                                        <i className="far fa-check-circle fs-25 text-success"></i>
                                                    </>
                                                : 
                                                    <>
                                                        <i className="fas fa-ban fs-25"></i>
                                                    </> 
                                                }
                                            </span>
                                        </li>
                                    </>
                                )
                            })}
                        </ul>

                        <br/>

                        {sLimit > totalData ? '': 
                            <>
                                <button className='btn btn-info' onClick={() => setSlimit(sLimit + 1)}>Load more data</button>
                            </>
                        }

                        
                    </div>
                </div>
                
                <div class="p-3 bg-light text-white rounded fixed-bottom" id="nav-edit-delete" style={{display: 'none'}}>
                    <div style={{textAlign: 'right'}}>
                        <i class="fas fa-times" style={{color: '#333', fontSize: '30px'}} onClick={() => colseButtonEditDelete()}></i>
                    </div>
                    <div class="d-grid mb-2">
                        <Link to={'edit/task/'+itemEdit} class="btn btn-info btn-block">
                            Edit
                        </Link>
                    </div>
                    <div class="d-grid">
                        <button type="button" class="btn btn-danger btn-block" onClick={() => deleteItem()}>Delete</button>
                    </div>
                </div>

            </div>
        </>
    )
};

export default Home;