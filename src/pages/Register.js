import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from 'axios';

const RegisterPage = () => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [age, setAge] = useState('')
    const [submitting, setSubmitting] = useState(false);
    // const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const doRegister = async (e) => {
        e.preventDefault();
        if(name === '') {
            alert('Pls insert name!') 
            return;
        }
        if(email === '') {
            alert('Pls insert email!') 
            return;
        }
        if(password === '') {
            alert('Pls insert password!')
            return;
        }
        if(age === '') {
            alert('Pls insert age!')
            return;
        }

        setSubmitting(true);
        let result = await requstPostAPI({name, email, password, age});
        if(result){
            navigate('/login')
        }
    }

    const requstPostAPI = async (datas) => {
        let res = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/register', datas, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(({data}) => {
            console.log(data)
            return data;
        })
        .catch(function (error) {
            // handle error
            console.log("error====");
            console.log(error);
        })
        return res;
    }

    return (
        <>
            <div className="container">
                <center>
                    <img src="logo512.png" style={styleCss.logo} />
                </center>
                <form onSubmit={doRegister}>
                    <div className="mb-3 mt-3">
                        <label htmlFor="name"> <span style={styleCss.requiredTex}>*</span> Name:</label>
                        <input autoComplete="off" type="text" className="form-control" 
                            id="name" placeholder="Enter name" name="name"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                    </div>
                    <div className="mb-3 mt-3">
                        <label htmlFor="email"> <span style={styleCss.requiredTex}>*</span> Email:</label>
                        <input autoComplete="off" type="email" className="form-control" 
                            id="email" placeholder="Enter email" name="email" 
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="pwd"> <span className="text-danger">*</span> Password:</label>
                        <input autoComplete="off" type="password" className="form-control" 
                            id="pwd" placeholder="Enter password" name="pswd" 
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <div className="mb-3 mt-3">
                        <label htmlFor="age"> <span style={styleCss.requiredTex}>*</span> Age:</label>
                        <input autoComplete="off" type="number" className="form-control" 
                            id="age" placeholder="Enter age" name="age" 
                            value={age}
                            onChange={(e) => setAge(e.target.value)} 
                        />
                    </div>
                    <div className="d-grid">
                        {submitting ? 
                        <>
                            <button className="btn btn-primary" disabled>
                                <span className="spinner-grow spinner-grow-sm"></span>
                                Loading..
                            </button>
                        </>
                        : 
                        <>
                            <button type="submit" className="btn btn-primary btn-block">Register</button>
                        </>}
                    </div>
                </form>
                <p>I have account! <Link to='/login'>Sign-In</Link> </p>
            </div>
            
        </>
    )
};

const styleCss = {
    logo: {
        width: '30%'
    },
    requiredTex: {
        color: 'red',
    }
}
  
export default RegisterPage;