import { useContext, useState } from "react";
import {  useNavigate } from "react-router-dom";
import { UserContext } from "../auth/Context";
import axios from 'axios';
import HeaderPage from "../inc/Header";

const CreateTaskPage = () => {
    const [description, setDescription] = useState('')
    const [date, setDate] = useState('')
    const [submitting, setSubmitting] = useState(false);
    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const createTask = async (e) => {
        e.preventDefault();
        if(description === ''){
            alert("Please enter the description!");
            return;
        }
        if(date === ''){
            alert("Please enter the date!");
            return;
        }

        setSubmitting(true);
        const resultTask = await requstCreateTaskAPI({description: description, createdAt: date});
        if(resultTask !== undefined && resultTask.success){
            navigate('/');
            setSubmitting(false);
        }
    }

    const requstCreateTaskAPI = async (datas) => {
        let res = await axios.post('https://api-nodejs-todolist.herokuapp.com/task', datas, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+user.token
            }
        }).then(({data}) => {
            console.log(data)
            return data;
        })
        .catch(function (error) {
            // handle error
            console.log("error====");
            console.log(error);
        })
        return res;
    }

    return (
        <>
            <div className="container-fluid">
                <HeaderPage title="Add TODO List" icon="fas fa-list" link="/" />

                <form onSubmit={createTask}>
                    <div className="mb-3 mt-3">
                        <label htmlFor="description"> <span style={styleCss.requiredTex}>*</span> Description:</label>
                        <input autoComplete="off" type="text" className="form-control" id="description" placeholder="Enter description" name="description"
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="date"> <span className="text-danger">*</span> Date:</label>
                        <input autoComplete="off" type="date" className="form-control" id="date" placeholder="Enter date" name="date"
                            value={date}
                            onChange={(e) => setDate(e.target.value)}
                        />
                    </div>
                    <div className="d-grid">
                        {submitting ? 
                            <>
                                <button className="btn btn-primary" disabled>
                                    <span className="spinner-grow spinner-grow-sm"></span>
                                    Submitting..
                                </button>
                            </>
                        : 
                        <>
                            <button type="submit" className="btn btn-primary btn-block">Save</button>
                        </>}
                    
                    </div>
                </form>
            </div>
            
        </>
    )
};

const styleCss = {
    logo: {
        width: '30%'
    },
    requiredTex: {
        color: 'red',
    }
}
  
export default CreateTaskPage;