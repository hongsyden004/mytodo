import $ from 'jquery';
import { useContext, useState, useEffect } from "react";
import {  useNavigate, useParams } from "react-router-dom";
import { UserContext } from "../auth/Context";
import axios from 'axios';
import HeaderPage from "../inc/Header";
var moment = require('moment'); // require

const EditTaskPage = () => {
    const [description, setDescription] = useState('')
    // const [date, setDate] = useState('')
    const [submitting, setSubmitting] = useState(false);
    const { user } = useContext(UserContext);
    const navigate = useNavigate();
    const { id } = useParams();

    useEffect(() => {
        const getTaskById = async () => {
            $('.loader').show();
            let res = await axios.get('https://api-nodejs-todolist.herokuapp.com/task/'+id, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+user.token
                }
            }).then(({data}) => {
                setDescription(data.data.description)
                // var dateFormat = moment(data.data.updatedAt).format('YYYY-MM-DD')
                // setDate(dateFormat);
                $('.loader').hide();
            })
            .catch(function (error) {
                // handle error
                console.log("error====");
                console.log(error);
            })
        }

        getTaskById();
    }, [user]);

    const updateTask = async (e) => {
        e.preventDefault();
        if(description === ''){
            alert("Please enter the description!");
            return;
        }

        setSubmitting(true);
        const resultTask = await requstEditTaskAPI({description: description});
        if(resultTask !== undefined && resultTask.success){
            navigate('/');
            setSubmitting(false);
        }
    }

    const requstEditTaskAPI = async (datas) => {
        if(datas)
        {
            let res = await axios.put(`https://api-nodejs-todolist.herokuapp.com/task/${id}`, datas, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+user.token
                }
            }).then(({data}) => {
                console.log(data)
                return data;
            })
            .catch(function (error) {
                // handle error
                console.log("error====");
                console.log(error);
            })
            return res;
        }
    }

    return (
        <>
            <div className="container-fluid">
                <HeaderPage title="Edit Task" icon="fas fa-list" link="/" />

                <form onSubmit={updateTask}>
                    <div className="mb-3 mt-3">
                        <label htmlFor="description"> <span style={styleCss.requiredTex}>*</span> Description:</label>
                        <input autoComplete="off" type="text" className="form-control" id="description" placeholder="Enter description" name="description"
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                        />
                    </div>
                    {/* <div className="mb-3">
                        <label htmlFor="date"> <span className="text-danger">*</span> Date:</label>
                        <input autoComplete="off" type="date" className="form-control" id="date" placeholder="Enter date" name="date"
                            value={date}
                            onChange={(e) => setDate(e.target.value)}
                        />
                    </div> */}
                    <div className="d-grid">
                        {submitting ? 
                            <>
                                <button className="btn btn-primary" disabled>
                                    <span className="spinner-grow spinner-grow-sm"></span>
                                    Submitting..
                                </button>
                            </>
                        : 
                        <>
                            <button type="submit" className="btn btn-primary btn-block">Update</button>
                        </>}
                    
                    </div>
                </form>
            </div>
            
        </>
    )
};

const styleCss = {
    logo: {
        width: '30%'
    },
    requiredTex: {
        color: 'red',
    }
}
  
export default EditTaskPage;