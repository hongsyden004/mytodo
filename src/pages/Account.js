import $ from 'jquery';
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../auth/Context";
import HeaderPage from "../inc/Header";
import axios from 'axios';

const Account = () => {
    const { user } = useContext(UserContext);
    const [name, setName] = useState(user.user.name)
    const [age, setAge] = useState(user.user.age)
    const [image, setImage] = useState(false);
    const [uploadImage, setUploadImage] = useState();
    useEffect(() => {
        const getUserImage = async () => {
            await axios.get('https://api-nodejs-todolist.herokuapp.com/user/'+user.user._id+'/avatar', {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+user.token
                }
            }).then(({data}) => {
                // console.log("data ===");
                // console.log(data);
                setImage('https://api-nodejs-todolist.herokuapp.com/user/'+user.user._id+'/avatar');
            })
            .catch(function (error) {
                // handle error
                console.log("error====");
                console.log(error);
            })
        }
        getUserImage();
    }, [user]);
    
    const doUploadImage = (event) => {
        setUploadImage(event[0]);
        $('#btn-upload-image').show();
        if (event[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#showimage').attr('src', e.target.result);
            };
            reader.readAsDataURL(event[0]);
        }
    }

    const uploadImageToAPI = async () => {
        $('.loader').show();
        var data = new FormData();
        data.append('avatar', uploadImage);
        let res = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/me/avatar', data, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+user.token
            }
        }).then(({data}) => {
            $('.loader').hide();
            $('#btn-upload-image').hide();
            setImage('https://api-nodejs-todolist.herokuapp.com/user/'+user.user._id+'/avatar');
            return data;
        })
        .catch(function (error) {
            // handle error
            console.log("error====");
            console.log(error);
            $('.loader').hide();
        })
        return res;
    }

    const clickImage = () => {
        $('#imageforupload').click();
    }

    return (
        <>
            <div className="container-fluid">
                <HeaderPage title="My Account" iconleft="fas fa-list" icon="fa-solid fa-right-from-bracket" logout={true} link="#" />
                <center>
                    <div className='mt-3' onClick={() => clickImage()}>
                        {image ? 
                            <>
                                <img id='showimage' src={image} className="rounded" alt="Cinque Terre" width={100}></img>
                            </> 
                        : 
                            <>
                                <i className="fa-solid fa-image" style={{fontSize: 40}} ></i>
                            </>
                        }
                    </div>
                    
                    <input type="file" id='imageforupload' className="form-control" name="image" onChange={(e) => doUploadImage(e.target.files)} style={{display: 'none'}} />
                    
                    <button id='btn-upload-image' type="button" className="btn btn-info btn-block mt-2" onClick={() => uploadImageToAPI()} style={{display: 'none'}}>Upload</button>
                </center>

                <div className="row mt-3">
                    <div className="col-md-12">
                        <ul class="list-group">
                            <li class="list-group-item">Email: {user.user.email} </li>
                        </ul>
                    </div>

                    <div className="col-md-12">
                        <form>
                            <div className="mb-3 mt-3">
                                <label htmlFor="name"> <span style={styleCss.requiredTex}>*</span> Name:</label>
                                <input autoComplete="off" type="text" className="form-control" id="name" placeholder="Enter name" name="name"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="age"> <span className="text-danger">*</span> Age:</label>
                                <input autoComplete="off" type="age" className="form-control" id="age" placeholder="Enter age" name="age"
                                    value={age}
                                    onChange={(e) => setAge(e.target.value)}
                                />
                            </div>
                            <div className="d-grid">
                                <button type="submit" className="btn btn-primary btn-block">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
};

const styleCss = {
    logo: {
        width: '30%'
    },
    requiredTex: {
        color: 'red',
    }
}
  
export default Account;