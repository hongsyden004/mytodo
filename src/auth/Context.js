import { createContext, useReducer, useEffect } from "react";

var userInfo = null;
if(localStorage.getItem('userInfo') !== null) {
    userInfo = JSON.parse(localStorage.getItem('userInfo'));
}
const userTodo = {
    user: userInfo,
    error: false,
    submitting: false
};

const reducer = (state, action) => {
    switch (action.type) {
        case "LOGIN_START":
            return { 
                user: null, 
                error: false, 
                submitting: true 
            };
        case "LOGIN_SUCESS":
            return { 
                user: action.userdata, 
                error: false, 
                submitting: false 
            };
        case "LOGIN_ERROR":
            return { 
                user: null, 
                error: true, 
                submitting: false 
            };
        case "LOGOUT":
                return { 
                    user: null, 
                    error: true, 
                    submitting: false 
                };
        default:
            return state;
    }
};
  

export const UserContext = createContext();

export const UserContextProvider = ({children}) => {
    const [state, dispatch] = useReducer(reducer, userTodo);

    useEffect(() => {
        localStorage.setItem('userInfo', JSON.stringify(state.user));
    }, [state.user]); // <- add the count variable here

    return (
        <UserContext.Provider value={{user: state.user, submitting: state.submitting , dispatch}}>
            {children}
        </UserContext.Provider>
    );
  }