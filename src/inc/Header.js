import { useContext } from "react";
import { Link } from "react-router-dom";
import { UserContext } from "../auth/Context";
import {  useNavigate } from "react-router-dom";
import axios from 'axios';

const HeaderPage = (props) => {
    const { user, dispatch } = useContext(UserContext);
    const navigate = useNavigate();

    const userLogout = async () => {
        let res = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/logout', {}, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+user.token
            }
        }).then(({data}) => {
            return data;
        })
        .catch(function (error) {
            // handle error
            console.log("error====");
            console.log(error);
        })

        if(res.success) 
        {
            dispatch({type: 'LOGOUT'});
            navigate('/')
        }
    }
    console.log(props);

    return (
        <>
            <div className="mt-2 p-2 bg-danger text-white rounded">
                <div className="row">
                    <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                            {props.iconleft ? 
                            <>
                                <Link to="/">
                                    <i className={`${props.iconleft} text-white`} style={{'fontSize': '35px'}}></i>
                                </Link>
                            </>
                            : 
                            <>
                                <Link to="/account">
                                    <i className="fas fa-user-circle text-white" style={{'fontSize': '35px'}}></i>
                                </Link>
                            </>
                            }

                    </div>
                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 text-center">
                        {props.title}
                    </div>
                    <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                        <div style={{float: 'right'}}>
                            {props.logout ? 
                                <>
                                    <Link to={props.link} onClick={() => userLogout()} >
                                        <i className={`${props.icon} text-white`} style={{'fontSize': '35px'}}></i>
                                    </Link> 
                                </> 
                            : 
                                <>
                                    <Link to={props.link}>
                                        <i className={`${props.icon} text-white`} style={{'fontSize': '35px'}}></i>
                                    </Link>
                                </>
                            }

                            
                        </div>
                    </div>
                </div>
            </div>
        </>
    )   
}

export default HeaderPage;